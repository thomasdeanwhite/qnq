package com.sourcecold.qnq;

import com.sourcecold.qnq.users.Professor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping(value = "/professor/create", method = RequestMethod.GET)
    public String create(Model model) {
        Professor professor = new Professor();
        model.addAttribute("professor", professor);

        return "professor/create";
    }

}
