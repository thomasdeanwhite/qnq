package com.sourcecold.qnq.users;

public class Professor {

    private String id;
    private String title;
    private String forename;
    private String surname;
    private String displayName;
    private String email;

    public Professor(){

    }

    public Professor(String id, String title, String forename, String surename, String displayName, String email) {
        this.id = id;
        this.title = title;
        this.forename = forename;
        this.surname = surename;
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
