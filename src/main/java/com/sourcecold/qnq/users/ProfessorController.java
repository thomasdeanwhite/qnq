package com.sourcecold.qnq.users;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/professor")
public class ProfessorController {

    @RequestMapping("/name/{name}")
    public Professor greeting(@PathVariable String name) {
        return new Professor(name, "Mr[s]", name, name, name, name);
    }

    @PostMapping
    public Professor newProfessor(@ModelAttribute("professor") Professor professor){
        return professor;
    }
}